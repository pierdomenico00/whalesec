#!/bin/bash
# Execute via root crontab

truncate -s 0 /storage/suricata/var/log/eve.json
truncate -s 0 /storage/suricata/var/log/stats.log
truncate -s 0 /storage/suricata/var/log/suricata.log

